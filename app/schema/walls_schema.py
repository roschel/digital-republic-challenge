from pydantic import BaseModel, Field


class WallsArea(BaseModel):
    height: float = Field(0, description="altura da parede")
    width: float = Field(0, description="largura da parede")
    quantity_doors: int = Field(
        0,
        description="quantidade de portas, sabendo que o m2 default é de 1.52 m2",
        alias="quantityDoors"
    )
    quantity_windows: int = Field(
        0,
        description="quantidade de janelas, sabendo que o m2 default é de 2.4 m2",
        alias="quantityWindows"
    )

    class Config:
        allow_population_by_field_name = True
