from typing import List

from pydantic import BaseModel, Field

from schema.walls_schema import WallsArea


class BodyIn(BaseModel):
    walls: List[WallsArea] = Field(None, description="Lista de dimensões das paredes em metros quadrados")

