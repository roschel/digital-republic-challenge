from enum import Enum


class PaintCanCapacity(Enum):
    CAPACITY = 5


class PaintCanVolumes(Enum):
    EIGTHEEN_LITERS = 18
    THREE_POINT_SIX_LITERS = 3.6
    TWO_POINT_FIVE_LITERS = 2.5
    HALF_LITER = 0.5


class DoorArea(Enum):
    M2 = 1.52
    HEIGHT = 1.9
    WIDTH = 0.8


class WindowArea(Enum):
    M2 = 2.4
    HEIGHT = 1.2
    WIDTH = 2.0
