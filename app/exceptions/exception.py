from fastapi import HTTPException


class InvalidAreaException(HTTPException):
    def __init__(self, content: str, status_code=400, *args):
        super(InvalidAreaException, self).__init__(
            status_code=status_code, detail=content, *args
        )


class DefaultExceptionsResponses:
    wall_area_invalid = "The wall with {} m2 is invalid."
    doors_and_windows_area_invalid = "The area of doors and windows ({} m2) are not acceptable"
    walldoor_height_invalid = "The difference between the door height and wall height is too small, lower than 30 cm"
