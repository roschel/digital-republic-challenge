from functools import lru_cache

from pydantic import BaseSettings, Field


class Settings(BaseSettings):
    ROOT_PATH: str = Field("")
    VERSION: str = Field("v0.0.0")
    ENVIRONMENT: str = Field("DEV")


@lru_cache
def get_settings():
    return Settings()


settings = get_settings()
