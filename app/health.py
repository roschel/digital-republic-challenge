from fastapi_health import health


def health_check():
    return True


api_health = health([health_check])
