from typing import List

from exceptions.exception import InvalidAreaException, DefaultExceptionsResponses
from schema.walls_schema import WallsArea
from utils.utils import DoorArea, WindowArea, PaintCanVolumes, PaintCanCapacity


def find_total_cans(total_liters: float):
    pass


def total_paint(m2: float) -> str:
    total_liters_init = m2 / PaintCanCapacity.CAPACITY.value
    total_can_needed = []

    total_liters_decre = total_liters_init

    for capacity in PaintCanVolumes:
        enough = False
        while not enough:
            if (total_liters_decre / capacity.value) > 1:
                total_can_needed.append(capacity.value)
                total_liters_decre -= capacity.value
            enough = (total_liters_decre / capacity.value) < 1

    if -1 < total_liters_decre < PaintCanVolumes.HALF_LITER.value:
        total_can_needed.append(PaintCanVolumes.HALF_LITER.value)

    number_of_occurences = list(set([(total_can_needed.count(i), i) for i in total_can_needed]))

    result = f"Here it is your shop list for a total of {total_liters_init} m2/L \n"

    for i in number_of_occurences:
        result += f"- {i[0]} can of {i[1]}L\n"

    return result


def check_doors_and_wall_heights(wall_height: float, doors: int) -> None:
    """
    This method helps us to validate if we have more than 30 cm between or door and wall
    :param wall_height:
    :param doors:
    :return:
    """
    if doors:
        wall_door_height = wall_height - DoorArea.HEIGHT.value
        if wall_door_height < 0.3:
            raise InvalidAreaException(
                content=DefaultExceptionsResponses.walldoor_height_invalid
            )
    return


def check_total_area_for_doors_and_windows(wall_area: float, doors: int, windows: int) -> float:
    """
    This method helps us to validate if the total area of doors and windows doesn't pass 50% of the total wall area
    :param wall_area:
    :param doors:
    :param windows:
    :return:
    """
    doors_and_windows_area = (doors * DoorArea.M2.value) + (windows * WindowArea.M2.value)

    if (doors_and_windows_area / wall_area) * 100 <= 50:
        return doors_and_windows_area
    else:
        raise InvalidAreaException(
            content=DefaultExceptionsResponses.doors_and_windows_area_invalid.format(doors_and_windows_area)
        )


def calculate_m2(height: float, width: float, doors: int, windows: int) -> float:
    # if this method below doesn't raise anything, it means tha we are good continue.
    # That's why it doesn't return anything.
    check_doors_and_wall_heights(wall_height=height, doors=doors)

    m2_wall_without_doors_and_windows = height * width

    doors_and_windows_area = check_total_area_for_doors_and_windows(
        wall_area=m2_wall_without_doors_and_windows,
        doors=doors,
        windows=windows
    )

    m2_wall = m2_wall_without_doors_and_windows - doors_and_windows_area

    if m2_wall < 1 or m2_wall > 50:
        raise InvalidAreaException(
            content=DefaultExceptionsResponses.wall_area_invalid.format(m2_wall)
        )

    return m2_wall


def calculate(walls: List[WallsArea]) -> str:
    m2 = 0.0
    for wall in walls:
        m2 += calculate_m2(
            height=wall.height,
            width=wall.width,
            doors=wall.quantity_doors,
            windows=wall.quantity_windows
        )

    return total_paint(m2)
