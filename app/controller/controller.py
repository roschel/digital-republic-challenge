from fastapi import APIRouter

from schema.schema import BodyIn
from service.service import calculate

route = APIRouter()


@route.post(
    "",
    description="Método responsável por calcular quantidade de tinta necessária para pintar uma sala",
    response_model=str,
    status_code=200,
)
async def paint_calculate(body: BodyIn):
    return calculate(walls=body.walls)
