from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

from config import settings
from controller import controller
from health import api_health

app = FastAPI(
    title="DIGITAL REPUBLIC CHALLENGE",
    description="API Responsável por calcular quantidade de tinta necessária para pintar uma sala",
    version=settings.VERSION,
    root_path=settings.ROOT_PATH
)

app.add_api_route("/health", api_health, name="Health Check", tags=["Health"])
app.include_router(controller.route, prefix="/calculate", tags=["Stats"])
