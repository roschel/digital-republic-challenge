uvicorn==0.16.0
fastapi==0.70.1
fastapi-health==0.4.0
python-dotenv==0.19.2