{
  "walls": [
    {
      "height": 2.3,
      "width": 15,
      "quantityDoors": 1,
      "quantityWindows": 0
    },
    {
      "height": 2.3,
      "width": 8,
      "quantityDoors": 0,
      "quantityWindows": 1
    },
    {
      "height": 2.3,
      "width": 15,
      "quantityDoors": 1,
      "quantityWindows": 2
    },
    {
      "height": 2.3,
      "width": 8,
      "quantityDoors": 1,
      "quantityWindows": 0
    }
  ]
}