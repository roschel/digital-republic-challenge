# Digital Republic Challenge

## How to run this project
Clone this repo into your machine: <br> 
`git clone git@gitlab.com:roschel/digital-republic-challenge.git`
 
Create the virtualenv so you can work safelly. Open your terminal and write: <br>
`virtualenv venv -p 3.9`

Now, activate the virtualenv: <br>
`source /venv/bin/activate`

Ok, let's install the dependencies: <br>
`pip install -r requirements.txt`

This enviroment variable is import to define the main path of the application: <br>
`export PYTHONPATH=app`

Now, we can start the server: <br>
`uvicorn app:app --reload --port 8000`


Open your browser at `http://localhost:8000/docs` and you will be able to see the swagger like the image below.
![image](https://user-images.githubusercontent.com/52433168/226774354-9d47f99b-5686-46c6-8f57-ede1b3d46325.png)


You can test the application by using the following body in the POST method `/calculate`
```json
{
  "walls": [
    {
      "height": 2.3,
      "width": 15,
      "quantityDoors": 1,
      "quantityWindows": 0
    },
    {
      "height": 2.3,
      "width": 8,
      "quantityDoors": 0,
      "quantityWindows": 1
    },
    {
      "height": 2.3,
      "width": 15,
      "quantityDoors": 1,
      "quantityWindows": 2
    },
    {
      "height": 2.3,
      "width": 8,
      "quantityDoors": 1,
      "quantityWindows": 0
    }
  ]
}
```
